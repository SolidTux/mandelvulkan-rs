#![allow(dead_code)]

#[macro_use]
extern crate vulkano;
#[macro_use]
extern crate vulkano_shader_derive;
extern crate winit;
extern crate vulkano_win;

use vulkano_win::VkSurfaceBuild;

use vulkano::buffer::BufferUsage;
use vulkano::buffer::CpuAccessibleBuffer;
use vulkano::buffer::CpuBufferPool;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::command_buffer::DynamicState;
use vulkano::device::Device;
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::framebuffer::Framebuffer;
use vulkano::framebuffer::Subpass;
use vulkano::instance::Instance;
use vulkano::pipeline::GraphicsPipeline;
use vulkano::pipeline::viewport::Viewport;
use vulkano::pipeline::viewport::Scissor;
use vulkano::swapchain;
use vulkano::swapchain::PresentMode;
use vulkano::swapchain::SurfaceTransform;
use vulkano::swapchain::Swapchain;
use vulkano::swapchain::AcquireError;
use vulkano::swapchain::SwapchainCreationError;
use vulkano::sync::now;
use vulkano::sync::GpuFuture;

use std::sync::Arc;
use std::mem;

fn main() {
    let instance = {
        let extensions = vulkano_win::required_extensions();

        Instance::new(None, &extensions, None).expect("failed to create Vulkan instance")
    };

    let physical = vulkano::instance::PhysicalDevice::enumerate(&instance)
        .next()
        .expect("no device available");
    println!(
        "Using device: {} (type: {:?})",
        physical.name(),
        physical.ty()
    );

    let mut events_loop = winit::EventsLoop::new();
    let window = winit::WindowBuilder::new()
        .build_vk_surface(&events_loop, instance.clone())
        .unwrap();

    let mut dimensions = {
        let (width, height) = window.window().get_inner_size_pixels().unwrap();
        [width, height]
    };

    let queue = physical
        .queue_families()
        .find(|&q| {
            q.supports_graphics() && window.surface().is_supported(q).unwrap_or(false)
        })
        .expect("couldn't find a graphical queue family");

    let (device, mut queues) = {
        let device_ext = vulkano::device::DeviceExtensions {
            khr_swapchain: true,
            ..vulkano::device::DeviceExtensions::none()
        };

        Device::new(
            physical,
            physical.supported_features(),
            &device_ext,
            [(queue, 0.5)].iter().cloned(),
        ).expect("failed to create device")
    };

    let queue = queues.next().unwrap();

    let (mut swapchain, mut images) = {
        let caps = window.surface().capabilities(physical).expect(
            "failed to get surface capabilities",
        );
        let alpha = caps.supported_composite_alpha.iter().next().unwrap();
        let format = caps.supported_formats[0].0;
        Swapchain::new(
            device.clone(),
            window.surface().clone(),
            caps.min_image_count,
            format,
            dimensions,
            1,
            caps.supported_usage_flags,
            &queue,
            SurfaceTransform::Identity,
            alpha,
            PresentMode::Fifo,
            true,
            None,
        ).expect("failed to create swapchain")
    };

    let vertex_buffer = {
        #[derive(Debug, Clone)]
        struct Vertex {
            position: [f32; 2],
        }
        impl_vertex!(Vertex, position);

        CpuAccessibleBuffer::from_iter(
            device.clone(),
            BufferUsage::all(),
            [
                Vertex { position: [-1., -1.] },
                Vertex { position: [1., -1.] },
                Vertex { position: [-1., 1.] },
                Vertex { position: [1., -1.] },
                Vertex { position: [-1., 1.] },
                Vertex { position: [1., 1.] },
            ].iter()
                .cloned(),
        ).expect("failed to create buffer")
    };


    let uniform_buffer = CpuBufferPool::<vs::ty::Data>::new(device.clone(), BufferUsage::all());
    let vs = vs::Shader::load(device.clone()).expect("failed to create shader module");
    let fs = fs::Shader::load(device.clone()).expect("failed to create shader module");

    let render_pass = Arc::new(
        single_pass_renderpass!(device.clone(),
        attachments: {
            color: {
                load: Clear,
                store: Store,
                format: swapchain.format(),
                samples: 1,
            }
        },
        pass: {
            color: [color],
            depth_stencil: {}
        }
    ).unwrap(),
    );

    let pipeline = Arc::new(
        GraphicsPipeline::start()
            .vertex_input_single_buffer()
            .vertex_shader(vs.main_entry_point(), ())
            .triangle_list()
            .viewports_scissors_dynamic(1)
            .fragment_shader(fs.main_entry_point(), ())
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .build(device.clone())
            .unwrap(),
    );

    let mut framebuffers: Option<Vec<Arc<vulkano::framebuffer::Framebuffer<_, _>>>> = None;

    let mut recreate_swapchain = false;

    let mut previous_frame_end = Box::new(now(device.clone())) as Box<GpuFuture>;

    let mut center_x = 0.;
    let mut center_y = 0.;
    let mut scale = 1.;
    let mut mouse_x = 0.;
    let mut mouse_y = 0.;
    loop {
        previous_frame_end.cleanup_finished();

        if recreate_swapchain {
            dimensions = {
                let (new_width, new_height) = window.window().get_inner_size_pixels().unwrap();
                [new_width, new_height]
            };

            let (new_swapchain, new_images) = match swapchain.recreate_with_dimension(dimensions) {
                Ok(r) => r,
                Err(SwapchainCreationError::UnsupportedDimensions) => {
                    continue;
                }
                Err(err) => panic!("{:?}", err),
            };

            mem::replace(&mut swapchain, new_swapchain);
            mem::replace(&mut images, new_images);

            framebuffers = None;

            recreate_swapchain = false;
        }

        if framebuffers.is_none() {
            let new_framebuffers = Some(
                images
                    .iter()
                    .map(|image| {
                        Arc::new(
                            Framebuffer::start(render_pass.clone())
                                .add(image.clone())
                                .unwrap()
                                .build()
                                .unwrap(),
                        )
                    })
                    .collect::<Vec<_>>(),
            );
            mem::replace(&mut framebuffers, new_framebuffers);
        }

        let uniform_buffer_subbuffer = {
            let data = vs::ty::Data {
                scale: scale,
                center: [center_x, center_y],
                _dummy0: [0; 4],
            };
            uniform_buffer.next(data).unwrap()
        };
        let set = Arc::new(
            PersistentDescriptorSet::start(pipeline.clone(), 0)
                .add_buffer(uniform_buffer_subbuffer)
                .unwrap()
                .build()
                .unwrap(),
        );

        let (image_num, acquire_future) =
            match swapchain::acquire_next_image(swapchain.clone(), None) {
                Ok(r) => r,
                Err(AcquireError::OutOfDate) => {
                    recreate_swapchain = true;
                    continue;
                }
                Err(err) => panic!("{:?}", err),
            };

        let command_buffer =
            AutoCommandBufferBuilder::primary_one_time_submit(device.clone(), queue.family())
                .unwrap()
                .begin_render_pass(
                    framebuffers.as_ref().unwrap()[image_num].clone(),
                    false,
                    vec![[0.0, 0.2, 0.0, 1.0].into()],
                )
                .unwrap()
                .draw(
                    pipeline.clone(),
                    DynamicState {
                        line_width: None,
                        viewports: Some(vec![
                            Viewport {
                                origin: [0., 0.],
                                dimensions: [dimensions[0] as f32, dimensions[1] as f32],
                                depth_range: 0.0..1.0,
                            },
                        ]),
                        scissors: Some(vec![
                            Scissor {
                                origin: [0, 0],
                                dimensions: dimensions,
                            },
                        ]),
                    },
                    vertex_buffer.clone(),
                    set.clone(),
                    (),
                )
                .unwrap()
                .end_render_pass()
                .unwrap()
                .build()
                .unwrap();

        let future = previous_frame_end
            .join(acquire_future)
            .then_execute(queue.clone(), command_buffer)
            .unwrap()
            .then_swapchain_present(queue.clone(), swapchain.clone(), image_num)
            .then_signal_fence_and_flush()
            .unwrap();
        previous_frame_end = Box::new(future) as Box<_>;

        let mut done = false;
        events_loop.poll_events(|ev| match ev {
            winit::Event::WindowEvent { event: winit::WindowEvent::Closed, .. } => done = true,
            winit::Event::WindowEvent { event: winit::WindowEvent::Resized(_, _), .. } => {
                recreate_swapchain = true
            }
            winit::Event::WindowEvent {
                event: winit::WindowEvent::MouseMoved { position: (px, py), .. }, ..
            } => {
                mouse_x = ((px as f32) / (dimensions[0] as f32) - 0.5) / scale;
                mouse_y = ((py as f32) / (dimensions[1] as f32) - 0.5) / scale;
            }
            winit::Event::WindowEvent {
                event: winit::WindowEvent::MouseInput {
                    state: winit::ElementState::Pressed,
                    button: winit::MouseButton::Left,
                    ..
                },
                ..
            } => {
                center_x -= mouse_x;
                center_y -= mouse_y;
            }
            winit::Event::WindowEvent {
                event: winit::WindowEvent::MouseInput {
                    state: winit::ElementState::Pressed,
                    button: winit::MouseButton::Right,
                    ..
                },
                ..
            } => {
                center_x = 0.;
                center_y = 0.;
                scale = 1.;
            }
            winit::Event::WindowEvent {
                event: winit::WindowEvent::KeyboardInput { input: inp, .. }, ..
            } => {
                match inp.virtual_keycode {
                    Some(winit::VirtualKeyCode::Escape) => done = true,
                    Some(winit::VirtualKeyCode::Right) => {
                        center_x -= 0.01 * scale;
                    }
                    Some(winit::VirtualKeyCode::Left) => {
                        center_x += 0.01 * scale;
                    }
                    Some(winit::VirtualKeyCode::Down) => {
                        center_y -= 0.01 * scale;
                    }
                    Some(winit::VirtualKeyCode::Up) => {
                        center_y += 0.01 * scale;
                    }
                    Some(winit::VirtualKeyCode::PageUp) => scale /= 1.05,
                    Some(winit::VirtualKeyCode::PageDown) => scale *= 1.05,
                    Some(_) => (),
                    None => (),
                }
            }
            _ => (),
        });
        if done {
            return;
        }
    }
}

mod vs {
    #[derive(VulkanoShader)]
    #[ty = "vertex"]
    #[src = "
#version 450
layout(location = 0) in vec2 position;
layout(location = 0) out vec2 pos;

layout(set = 0, binding = 0) uniform Data {
    float scale;
    vec2 center;
} uniforms;

void main() {
    pos = uniforms.scale*position - uniforms.center;
    gl_Position = vec4(position, 0.0, 1.0);
}
"]
    struct Dummy;
}

mod fs {
    #[derive(VulkanoShader)]
    #[ty = "fragment"]
    #[src = "
#version 450

layout(location = 0) in vec2 pos;
layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0) uniform Data {
    float scale;
    vec2 center;
} uniforms;

const vec3 colormap[33] = vec3[33](
    vec3(0.267004, 0.004874, 0.329415),
    vec3(0.277018, 0.050344, 0.375715),
    vec3(0.282327, 0.094955, 0.417331),
    vec3(0.282884, 0.135920, 0.453427),
    vec3(0.278826, 0.175490, 0.483397),
    vec3(0.270595, 0.214069, 0.507052),
    vec3(0.258965, 0.251537, 0.524736),
    vec3(0.244972, 0.287675, 0.537260),
    vec3(0.229739, 0.322361, 0.545706),
    vec3(0.214298, 0.355619, 0.551184),
    vec3(0.199430, 0.387607, 0.554642),
    vec3(0.185556, 0.418570, 0.556753),
    vec3(0.172719, 0.448791, 0.557885),
    vec3(0.160665, 0.478540, 0.558115),
    vec3(0.149039, 0.508051, 0.557250),
    vec3(0.137770, 0.537492, 0.554906),
    vec3(0.127568, 0.566949, 0.550556),
    vec3(0.120565, 0.596422, 0.543611),
    vec3(0.120638, 0.625828, 0.533488),
    vec3(0.132268, 0.655014, 0.519661),
    vec3(0.157851, 0.683765, 0.501686),
    vec3(0.196571, 0.711827, 0.479221),
    vec3(0.246070, 0.738910, 0.452024),
    vec3(0.304148, 0.764704, 0.419943),
    vec3(0.369214, 0.788888, 0.382914),
    vec3(0.440137, 0.811138, 0.340967),
    vec3(0.515992, 0.831158, 0.294279),
    vec3(0.595839, 0.848717, 0.243329),
    vec3(0.678489, 0.863742, 0.189503),
    vec3(0.762373, 0.876424, 0.137064),
    vec3(0.845561, 0.887322, 0.099702),
    vec3(0.926106, 0.897330, 0.104071),
    vec3(0., 0., 0.)
);

void main() {
    vec2 c = pos;
    c.x *= 2;
    vec2 z = vec2(0.0, 0.0);
    int ind = 0;
    for (ind = 0; ind < 33; ind++) {
        z = vec2(
            z.x * z.x - z.y * z.y + c.x,
            z.y * z.x + z.x * z.y + c.y
        );

        if (length(z) > 2.0) {
            break;
        }
    }
    f_color = vec4(
        colormap[ind],
        1.);
}
"]
    struct Dummy;
}
